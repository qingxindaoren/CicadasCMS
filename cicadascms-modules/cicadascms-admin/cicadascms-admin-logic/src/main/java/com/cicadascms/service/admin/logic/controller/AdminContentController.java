package com.cicadascms.service.admin.logic.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.service.admin.logic.dto.ContentInputDTO;
import com.cicadascms.service.admin.logic.dto.ContentQueryDTO;
import com.cicadascms.service.admin.logic.dto.ContentUpdateDTO;
import com.cicadascms.service.admin.logic.service.IAdminContentService;
import com.cicadascms.service.admin.logic.vo.ContentVO;
import com.cicadascms.common.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


/**
 * <p>
 * 内容 控制器
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Api(tags = "A-内容管理接口")
@RestController
@RequestMapping("/admin/cms/content")
@AllArgsConstructor
public class AdminContentController {
    private final IAdminContentService contentService;

    @ApiOperation(value = "内容分页接口")
    @GetMapping("/list")
    public R<Page<ContentVO>> page(ContentQueryDTO contentQueryDTO) {
        return contentService.page(contentQueryDTO);
    }

    @ApiOperation(value = "内容保存接口")
    @PostMapping
    public R<Boolean> save(@Valid @RequestBody ContentInputDTO contentInputDTO) {
        return contentService.save(contentInputDTO);
    }

    @ApiOperation(value = "内容更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid @RequestBody ContentUpdateDTO contentUpdateDTO) {
        return contentService.update(contentUpdateDTO);
    }

    @ApiOperation(value = "内容详情接口")
    @GetMapping("/{id}")
    public R<ContentVO> getById(@PathVariable Long id) {
        return contentService.findById(id);
    }

    @ApiOperation(value = "内容删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return contentService.deleteById(id);
    }


}
