package com.cicadascms.service.admin.logic.vo;

import com.cicadascms.common.tree.TreeNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * ChannelVO对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ChannelVO对象", description = "栏目")
public class ChannelVO implements Serializable, TreeNode<ChannelVO> {

    private static final long serialVersionUID = 1L;

    private Integer channelId;
    /**
     * 站点编号
     */
    @ApiModelProperty(value = "2-站点编号")
    private Integer siteId;

    /**
     * 域名
     */
    @ApiModelProperty(value = "2-栏目域名")
    private String domain;

    /**
     * 分类明细
     */
    @ApiModelProperty(value = "3-分类明细")
    private String channelName;
    /**
     * 栏目模型编号
     */
    @ApiModelProperty(value = "4-栏目模型编号")
    private Integer channelModelId;
    /**
     * 内容模型编号
     */
    @ApiModelProperty(value = "5-内容模型编号")
    private String contentModelIds;
    /**
     * 栏目路径
     */
    @ApiModelProperty(value = "6-栏目路径")
    private String channelUrlPath;
    /**
     * 父类编号
     */
    @ApiModelProperty(value = "7-父类编号")
    private Long parentId;
    /**
     * 单页栏目（0：不是，1：是）
     */
    @ApiModelProperty(value = "8-单页栏目（0：不是，1：是）")
    private Boolean isAlone;
    /**
     * 单页内容
     */
    @ApiModelProperty(value = "9-单页内容")
    private String aloneContent;
    /**
     * 首页视图模板
     */
    @ApiModelProperty(value = "10-首页视图模板")
    private String indexView;
    /**
     * 列表页视图模板
     */
    @ApiModelProperty(value = "11-列表页视图模板")
    private String listView;
    /**
     * 内容页视图模板
     */
    @ApiModelProperty(value = "12-内容页视图模板")
    private String contentView;
    /**
     * 导航
     */
    @ApiModelProperty(value = "13-导航")
    private Boolean isNav;
    /**
     * 外链地址
     */
    @ApiModelProperty(value = "14-外链地址")
    private String url;
    /**
     * 是否有子类
     */
    @ApiModelProperty(value = "15-是否有子类")
    private Boolean hasChildren;
    /**
     * 栏目分页数量
     */
    @ApiModelProperty(value = "16-栏目分页数量")
    private Integer pageSize;
    /**
     * 当前栏目下的是否支持全文搜索
     */
    @ApiModelProperty(value = "17-当前栏目下的是否支持全文搜索")
    private Boolean allowSearch;
    /**
     * 栏目分类
     */
    @ApiModelProperty(value = "18-栏目分类")
    private Integer channelType;
    /**
     * 栏目图标
     */
    @ApiModelProperty(value = "19-栏目图标")
    private String channelIcon;
    private Integer sortId;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "21-创建人")
    private Integer createUser;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "22-创建时间")
    private LocalDateTime createTime;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "23-更新用户")
    private Integer updateUser;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "24-更新时间")
    private LocalDateTime updateTime;

    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "19-扩展字段")
    private List<ModelFieldValueVO> ext;

    String parentName;
    List<ChannelVO> children;

    public void setChildren(List<ChannelVO> children) {
        this.children = children;
    }

    @Override
    public Serializable getCurrentNodeId() {
        return getChannelId();
    }

    @Override
    public Serializable getParentNodeId() {
        return getParentId();
    }

}
