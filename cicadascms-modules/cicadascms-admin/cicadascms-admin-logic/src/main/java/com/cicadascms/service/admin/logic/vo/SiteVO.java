package com.cicadascms.service.admin.logic.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * CmsSiteVO对象
 * </p>
 *
 * @author jin
 * @since 2020-10-12
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CmsSiteVO对象", description="站点表")
public class SiteVO  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer siteId;
    /**
    * 站点名称
    */
    @ApiModelProperty(value = "2-站点名称" )
    private String siteName;
    /**
    * http协议
    */
    @ApiModelProperty(value = "3-http协议" )
    private Integer httpProtocol;
    /**
    * 站点域名
    */
    @ApiModelProperty(value = "4-站点域名" )
    private String domain;
    /**
    * 站点路径
    */
    @ApiModelProperty(value = "5-站点路径" )
    private String siteDir;
    /**
    * 站点状态
    */
    @ApiModelProperty(value = "6-站点状态" )
    private Boolean status;
    /**
    * 站点请求后缀
    */
    @ApiModelProperty(value = "7-站点请求后缀" )
    private Integer siteSuffix;
    /**
    * 是否默认站点
    */
    @ApiModelProperty(value = "8-是否默认站点" )
    private Boolean isDefault;
    /**
    * pc端模板目录
    */
    @ApiModelProperty(value = "9-pc端模板目录" )
    private String pcTemplateDir;
    /**
    * 移动端手机模板
    */
    @ApiModelProperty(value = "10-移动端手机模板" )
    private String mobileTemplateDir;

    @ApiModelProperty(value = "11-创建时间" )
    private LocalDateTime createTime;

    @ApiModelProperty(value = "12-更新时间" )
    private LocalDateTime updateTime;


}
