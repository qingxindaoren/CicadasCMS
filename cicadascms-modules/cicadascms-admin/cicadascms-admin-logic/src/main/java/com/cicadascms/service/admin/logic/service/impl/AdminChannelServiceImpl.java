package com.cicadascms.service.admin.logic.service.impl;


import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.service.admin.logic.dto.ChannelInputDTO;
import com.cicadascms.service.admin.logic.dto.ChannelQueryDTO;
import com.cicadascms.service.admin.logic.dto.ChannelUpdateDTO;
import com.cicadascms.service.admin.logic.dto.ModelFieldValueDTO;
import com.cicadascms.service.admin.logic.vo.ChannelVO;
import com.cicadascms.service.admin.logic.wrapper.ChannelWrapper;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.DbUtils;
import com.cicadascms.data.domain.*;
import com.cicadascms.data.mapper.ChannelMapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.mapper.ContentMapper;
import com.cicadascms.service.admin.logic.service.IAdminChannelService;
import com.cicadascms.service.admin.logic.service.IAdminModelFieldService;
import com.cicadascms.service.admin.logic.service.IAdminModelService;
import com.cicadascms.support.datamodel.DataModelSqlBuilder;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.cicadascms.service.admin.logic.service.impl.AdminModelServiceImpl.DEFAULT_CHANNEL_ID;

/**
 * <p>
 * 栏目 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
@AllArgsConstructor
public class AdminChannelServiceImpl extends BaseService<ChannelMapper, ChannelDO> implements IAdminChannelService {
    private final IAdminModelService modelService;
    private final IAdminModelFieldService modelFieldService;
    private final DataSource dataSource;
    private final ContentMapper contentMapper;

    @Override
    public R<Page<ChannelVO>> page(ChannelQueryDTO channelQueryDTO) {
        ChannelDO channelDO = channelQueryDTO.convertToEntity();
        Page<ChannelDO> page = baseMapper.selectPage(channelQueryDTO.page(), getLambdaQueryWrapper().setEntity(channelDO));
        return R.ok(ChannelWrapper.newBuilder().pageVO(page));
    }

    private void fillModelFieldKeyAndValue(ModelDO modelDO, Map<String, Object> fieldKeyAndValue, List<ModelFieldValueDTO> modelFieldValueDTOS) {
        List<ModelFieldDO> modelFieldDOS = modelFieldService.findByModelId(modelDO.getModelId());
        if (Fn.isEmpty(modelFieldDOS)) throw new ServiceException("模型已被删除或不存在！");
        //遍历填充
        modelFieldDOS.stream().parallel().forEach(modelFieldDO -> {
            //获取模型字段值
            ModelFieldValue modelFieldValue = modelFieldValueDTOS
                    .stream()
                    .filter(e -> Fn.equal(e.getFieldName(), modelFieldDO.getFieldName()))
                    .findFirst()
                    .orElseThrow(new ServiceException("字段名称有误！"))
                    .getFieldValue();
            //有数据填充
            if (Fn.isNotNull(modelFieldValue))
                fieldKeyAndValue.put(modelFieldDO.getFieldName(), Fn.toJson(modelFieldValue));
        });
    }

    @Transactional
    @Override
    public R<Boolean> save(ChannelInputDTO channelInputDTO) {
        ChannelDO channelDO = channelInputDTO.convertToEntity();
        if (checkAndSave(channelDO) && Fn.isNotEmpty(channelInputDTO.getExt())) {
            ModelDO modelDO = modelService.getById(channelDO.getChannelModelId());
            Map<String, Object> fieldKeyAndValue = new HashMap<>();
            //遍历填充模型字段
            fillModelFieldKeyAndValue(modelDO, fieldKeyAndValue, channelInputDTO.getExt());
            fieldKeyAndValue.put(DEFAULT_CHANNEL_ID, channelDO.getChannelId());
            //构建插入语句
            String insertSql = DataModelSqlBuilder.newBuilder()
                    .newInsertDataSqlBuilder()
                    .tableName(modelDO.getTableName())
                    .insert(new ArrayList<>(fieldKeyAndValue.keySet()),
                            new ArrayList<>(fieldKeyAndValue.values()))
                    .buildSql();
            DbUtils.use(dataSource).exec(insertSql);
        }
        return R.ok("新增栏目成功！", true);
    }

    private boolean checkAndSave(ChannelDO channelDO) {
        ChannelDO parentChannel = null;
        if (Fn.isNotNull(channelDO.getParentId())) {
            if (Fn.notEqual(Constant.PARENT_ID, channelDO.getParentId())) {
                parentChannel = getById(channelDO.getParentId());
                if (parentChannel == null) throw new ServiceException("上级栏目选择有误！");
            }
        } else {
            channelDO.setParentId(Constant.PARENT_ID);
        }
        if (Fn.isNotNull(parentChannel)) {
            parentChannel.setHasChildren(true);
            updateById(parentChannel);
        }
        return save(channelDO);
    }

    @Transactional
    @Override
    public R<Boolean> update(ChannelUpdateDTO channelUpdateDTO) {
        ChannelDO channelDO = channelUpdateDTO.convertToEntity();
        if (checkAndUpdate(channelDO) && Fn.isNotEmpty(channelUpdateDTO.getExt())) {
            ModelDO modelDO = modelService.getById(channelDO.getChannelModelId());
            Map<String, Object> fieldKeyAndValue = new HashMap<>();
            //遍历填充模型字段
            fillModelFieldKeyAndValue(modelDO, fieldKeyAndValue, channelUpdateDTO.getExt());
            //构建更新语句
            String updateSql = DataModelSqlBuilder.newBuilder()
                    .newUpdateDateSqlBuilder()
                    .tableName(modelDO.getTableName())
                    .update(DEFAULT_CHANNEL_ID, channelUpdateDTO.getChannelId(),
                            new ArrayList<>(fieldKeyAndValue.keySet()),
                            new ArrayList<>(fieldKeyAndValue.values()))
                    .buildSql();
            DbUtils.use(dataSource).exec(updateSql);
        }
        return R.ok("栏目更新成功！", true);
    }

    private boolean checkAndUpdate(ChannelDO channelDO) {
        ChannelDO currentChannel = getById(channelDO.getChannelId());

        if (Fn.isNull(currentChannel)) throw new ServiceException("参数错误！");
        if (Fn.equal(channelDO.getParentId(), currentChannel.getChannelId())) throw new ServiceException("上级栏目不能为为自己！");

        List<ChannelDO> currentChannelChildList = findByParentId(currentChannel.getChannelId());
        if (Fn.isNotNull(channelDO.getParentId()) && Fn.notEqual(Constant.PARENT_ID, channelDO.getParentId()) && Fn.notEqual(channelDO.getParentId(), currentChannel.getParentId())) {
            ChannelDO currentParentChannel = getById(channelDO.getParentId());
            if (currentParentChannel == null) throw new ServiceException("上级栏目不存在！");
            if (Fn.isNotEmpty(currentChannelChildList)) {
                currentChannelChildList.parallelStream().forEach(e -> {
                    if (Fn.equal(e.getParentId(), currentParentChannel.getParentId())) {
                        throw new ServiceException("上级栏目选择有误！");
                    }
                });
            }
            currentParentChannel.setHasChildren(true);
            updateById(currentParentChannel);
        }
        ChannelDO quondamParentChannel = getById(currentChannel.getParentId());
        if (Fn.isNotNull(quondamParentChannel)) {
            List<ChannelDO> parentChannelChildList = findByParentId(currentChannel.getParentId());
            if (CollectionUtil.isEmpty(parentChannelChildList)) {
                quondamParentChannel.setHasChildren(false);
                updateById(quondamParentChannel);
            }
        }
        //Jin 判断当前栏目是否拥有下级栏目
        channelDO.setHasChildren(Fn.isNotEmpty(currentChannelChildList));
        return updateById(channelDO);

    }

    @Override
    public R<ChannelVO> findById(Serializable id) {
        ChannelDO channelDO = baseMapper.selectById(id);
        return R.ok(ChannelWrapper.newBuilder().entityVO(channelDO));

    }

    @Transactional
    @Override
    public R<Boolean> deleteById(Serializable id) {
        LambdaQueryWrapper<ChannelDO> queryWrapper = getLambdaQueryWrapper().eq(ChannelDO::getParentId, id);
        Integer count = baseMapper.selectCount(queryWrapper);
        if (count > 0) {
            throw new ServiceException("请先删除子栏目后再操作！");
        }

        ChannelDO channel = getById(id);

        if (Fn.isNull(channel)) {
            throw new ServiceException("栏目不存在！");
        }

        Integer contentCount = contentMapper.selectCount(new LambdaQueryWrapper<ContentDO>().eq(ContentDO::getChannelId, id));

        if (contentCount > 0) {
            throw new ServiceException("当前栏目下有" + contentCount + "条内容，清先删除内容后再操作！");
        }

        removeById(id);

        if (Fn.isNotNull(channel.getParentId()) && Fn.notEqual(Constant.PARENT_ID, channel.getParentId())) {
            Integer childCount = baseMapper.selectCount(getLambdaQueryWrapper().eq(ChannelDO::getParentId, channel.getParentId()));
            //更新上级栏目状态
            if (childCount <= 0) {
                ChannelDO parentChannel = getById(channel.getParentId());
                parentChannel.setHasChildren(false);
                updateById(parentChannel);
            }
        }
        return R.ok(true);
    }

    @Override
    public ChannelVO findBySiteIdAndChannelUrlPath(Integer siteId, String channelUrlPath) {
        ChannelDO channelDO = baseMapper.selectOne(getLambdaQueryWrapper()
                .eq(ChannelDO::getSiteId, siteId)
                .eq(ChannelDO::getChannelUrlPath, channelUrlPath));
        return ChannelWrapper.newBuilder().entityVO(channelDO);
    }

    @Override
    public ChannelDO findByDomain(String domain) {
        return super.getOne(getLambdaQueryWrapper().eq(ChannelDO::getDomain, domain));
    }

    @Override
    public List<ChannelVO> getTree(Integer siteId) {
        List<ChannelDO> channelDOS = baseMapper.selectList(getLambdaQueryWrapper()
                .eq(ChannelDO::getSiteId, siteId)
                .orderByAsc(ChannelDO::getSortId));
        List<ChannelVO> channel = ChannelWrapper.newBuilder().listVO(channelDOS);
        return ChannelWrapper.newBuilder().treeVO(channel);
    }

    @Override
    public List<ChannelDO> findByParentId(Integer parentId) {
        return baseMapper.selectList(getLambdaQueryWrapper().eq(ChannelDO::getParentId, parentId));
    }

    @Override
    public Map<String, Object> findByTableNameAndChannelId(String fields, String tableName, Integer channelId) {
        return baseMapper.selectByTableNameAndChannelId(fields, tableName, channelId);
    }

    @Override
    protected String getCacheName() {
        return "channelCache";
    }


}
