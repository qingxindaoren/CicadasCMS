package com.cicadascms.service.admin.logic.service.impl;


import com.cicadascms.common.utils.ResUtils;
import com.cicadascms.service.admin.logic.dto.TemplateFileInputDTO;
import com.cicadascms.service.admin.logic.dto.TemplateFileUpdateDTO;
import com.cicadascms.service.admin.logic.vo.TemplateFileVO;
import com.cicadascms.service.admin.logic.service.IAdminTemplateService;

import java.io.File;
import java.util.List;

/**
 * <p>
 * 模管理 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public class AdminTemplateServiceImpl implements IAdminTemplateService {

    private static final String TEMPLATES_DATA_PATH = ResUtils.RES_DATA_PATH + "templates" + File.separator + "www";

    @Override
    public boolean saveTemplateFile(TemplateFileInputDTO templateFileInputDTO) {
        return false;
    }

    @Override
    public boolean updateTemplateFile(TemplateFileUpdateDTO templateFileUpdateDTO) {
        return false;
    }

    @Override
    public boolean deleteTemplateFile(String filePath) {
        return false;
    }

    @Override
    public List<TemplateFileVO> getTemplateFileList(String filePath) {


        return null;
    }

    @Override
    public TemplateFileVO find(String fileName) {
        return null;
    }
}
