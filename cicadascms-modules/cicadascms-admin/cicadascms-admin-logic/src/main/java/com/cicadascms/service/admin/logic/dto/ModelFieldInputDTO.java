package com.cicadascms.service.admin.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.support.datamodel.modelfield.ModelFieldProp;
import com.cicadascms.support.datamodel.modelfield.ModelFieldRule;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * ModelFieldInputDTO对象
 * 模型字段
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "InputModelFieldDTO对象")
public class ModelFieldInputDTO extends BaseDTO<ModelFieldInputDTO, ModelFieldDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 模型字段
     */
    @ApiModelProperty(value = "1-模型字段")
    private Integer modelId;
    /**
     * 字段名称
     */
    @ApiModelProperty(value = "2-字段名称")
    private String fieldName;
    /**
     * 字段类型
     */
    @ApiModelProperty(value = "3-字段类型")
    private Integer fieldType;
    /**
     * 字段规则
     */
    @ApiModelProperty(value = "5-字段规则")
    private ModelFieldProp<ModelFieldRule, ModelFieldValue> fieldProp;
    /**
     * 是否检索字段
     */
    @ApiModelProperty(value = "6-是否检索字段")
    private Boolean isSearchField;
    /**
     * 字段描述
     */
    @ApiModelProperty(value = "7-字段描述")
    private String des;

    public static Converter<ModelFieldInputDTO, ModelFieldDO> converter = new Converter<ModelFieldInputDTO, ModelFieldDO>() {
        @Override
        public ModelFieldDO doForward(ModelFieldInputDTO modelFieldInputDTO) {
            ModelFieldDO modelFieldDO = WarpsUtils.copyTo(modelFieldInputDTO, ModelFieldDO.class);
            assert modelFieldDO != null;
            modelFieldDO.setFieldConfig(Fn.toJson(modelFieldInputDTO.getFieldProp()));
            return modelFieldDO;
        }

        @Override
        public ModelFieldInputDTO doBackward(ModelFieldDO modelFieldDO) {
            return WarpsUtils.copyTo(modelFieldDO, ModelFieldInputDTO.class);
        }
    };

    @Override
    public ModelFieldDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ModelFieldInputDTO convertFor(ModelFieldDO modelFieldDO) {
        return converter.doBackward(modelFieldDO);
    }
}
