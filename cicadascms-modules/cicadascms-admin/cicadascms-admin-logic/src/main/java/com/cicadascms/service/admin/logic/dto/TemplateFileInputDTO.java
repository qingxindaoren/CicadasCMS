package com.cicadascms.service.admin.logic.dto;

import lombok.Data;
import lombok.ToString;

/**
 * TemplateFileInputDTO
 *
 * @author Jin
 */
@Data
@ToString
public class TemplateFileInputDTO {

    private String fileName;
    private String filePath;
    private String fileContent;

}
