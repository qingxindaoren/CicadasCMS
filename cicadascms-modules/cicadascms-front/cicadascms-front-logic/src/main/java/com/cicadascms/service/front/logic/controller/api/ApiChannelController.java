package com.cicadascms.service.front.logic.controller.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "前台栏目接口")
@RestController
@RequestMapping("/api/channel")
public class ApiChannelController {

    @GetMapping("/")
    public String index() {
        return "ok";
    }

}
