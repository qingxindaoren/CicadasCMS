package com.cicadascms.service.front.logic.controller.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "前台检索接口")
@RestController
@RequestMapping("/api/search")
public class ApiSearchController {

    @GetMapping("/")
    public String index() {
        return "ok";
    }

}
