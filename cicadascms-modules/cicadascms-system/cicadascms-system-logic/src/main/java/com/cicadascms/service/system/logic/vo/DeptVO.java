package com.cicadascms.service.system.logic.vo;
import com.cicadascms.common.tree.TreeNode;
import com.cicadascms.data.domain.DeptDO;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "部门", description = "部门表")
public class DeptVO extends DeptDO implements TreeNode<DeptVO> {
    private String parentName;
    private List<DeptVO> children;

    @Override
    public Serializable getCurrentNodeId() {
        return getDeptId();
    }

    @Override
    public Serializable getParentNodeId() {
        return getParentId();
    }

}
