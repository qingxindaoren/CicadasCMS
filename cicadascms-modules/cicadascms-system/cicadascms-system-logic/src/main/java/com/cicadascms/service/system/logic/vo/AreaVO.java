package com.cicadascms.service.system.logic.vo;

import com.cicadascms.common.tree.TreeNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * SysAreaVO对象
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Getter
@Setter
@ApiModel(value = "SysAreaVO对象", description = "区域")
public class AreaVO implements Serializable, TreeNode<AreaVO> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 名称
     */
    @ApiModelProperty(value = "2-名称")
    private String name;
    /**
     * 父编号
     */
    @ApiModelProperty(value = "3-父编号")
    private String parentId;
    /**
     * 简称
     */
    @ApiModelProperty(value = "4-简称")
    private String shortName;
    /**
     * 级别
     */
    @ApiModelProperty(value = "5-级别")
    private Integer levelType;
    /**
     * 城市代码
     */
    @ApiModelProperty(value = "6-城市代码")
    private String ctyCode;
    /**
     * 邮编
     */
    @ApiModelProperty(value = "7-邮编")
    private String zipCode;
    /**
     * 详细名称
     */
    @ApiModelProperty(value = "8-详细名称")
    private String mergerName;
    /**
     * 经度
     */
    @ApiModelProperty(value = "9-经度")
    private String lng;
    /**
     * 维度
     */
    @ApiModelProperty(value = "10-维度")
    private String lat;
    /**
     * 拼音
     */
    @ApiModelProperty(value = "11-拼音")
    private String pinyin;

    private List<AreaVO> children;


    private String parentName;

    private boolean leaf;

    @Override
    public Serializable getCurrentNodeId() {
        return getId();
    }

    @Override
    public Serializable getParentNodeId() {
        return getParentId();
    }

    @Override
    public void setChildren(List<AreaVO> children) {
        this.children = children;
    }


}
