package com.cicadascms.service.system.logic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.AreaDO;
import com.cicadascms.service.system.logic.dto.AreaInputDTO;
import com.cicadascms.service.system.logic.dto.AreaQueryDTO;
import com.cicadascms.service.system.logic.dto.AreaUpdateDTO;
import com.cicadascms.service.system.logic.vo.AreaVO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 区域 服务类
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
public interface IAreaService extends IService<AreaDO> {

    List<AreaVO> getTree();
    List<AreaVO> getTree(String parentCode);

    /**
     * 分页方法
     * @param areaQueryDTO
     * @return
     */
    R list(AreaQueryDTO areaQueryDTO);

    /**
     * 保存方法
     * @param areaInputDTO
     * @return
     */
    R save(AreaInputDTO areaInputDTO);

    /**
     * 更新方法
     * @param areaUpdateDTO
     * @return
     */
    R update(AreaUpdateDTO areaUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
