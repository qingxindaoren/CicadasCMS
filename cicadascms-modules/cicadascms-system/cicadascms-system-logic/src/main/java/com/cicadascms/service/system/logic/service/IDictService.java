package com.cicadascms.service.system.logic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.DictDO;
import com.cicadascms.service.system.logic.dto.DictInputDTO;
import com.cicadascms.service.system.logic.dto.DictQueryDTO;
import com.cicadascms.service.system.logic.dto.DictUpdateDTO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author jin
 * @since 2020-04-07
 */
public interface IDictService extends IService<DictDO> {

    /**
     * 分页方法
     *
     * @param dictQueryDTO
     * @return
     */
    R findList(DictQueryDTO dictQueryDTO);

    /**
     * 分页方法
     *
     * @param dictQueryDTO
     * @return
     */
    R page(DictQueryDTO dictQueryDTO);

    /**
     * 保存方法
     *
     * @param dictInputDTO
     * @return
     */
    R save(DictInputDTO dictInputDTO);

    /**
     * 更新方法
     *
     * @param dictUpdateDTO
     * @return
     */
    R update(DictUpdateDTO dictUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R deleteById(Serializable id);

    /**
     * 根据parentId获取字典
     *
     * @param id
     * @return
     */
    List<DictDO> findByParentId(Serializable id);

    /**
     * 根据code 和 parentId 查询
     *
     * @param code
     * @param parentId
     * @return
     */
    List<DictDO> findByCodeAndParentId(String code, Serializable parentId);
    /**
     * 根据code
     *
     * @param code
     * @return
     */
    DictDO findOneByCode(String code);

    /**
     * 根据字典key和value获取字典详情
     *
     * @param dictCode
     * @param dictValue
     * @return
     */
    DictDO findByCodeAndValue(String dictCode, String dictValue);
}
