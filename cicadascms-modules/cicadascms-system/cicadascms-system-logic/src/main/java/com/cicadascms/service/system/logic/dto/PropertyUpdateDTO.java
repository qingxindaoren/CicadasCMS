package com.cicadascms.service.system.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.PropertyDO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * PropertyDOUpdateDTO对象
 *
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "PropertyUpdateDTO对象")
public class PropertyUpdateDTO extends BaseDTO<PropertyUpdateDTO, PropertyDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @NotBlank(message = "参数名称不能为空！")
    private String name;
    @NotBlank(message = "参数键名不能为空")
    private String propertyName;
    @NotBlank(message = "参数键值不能为空")
    private String propertyValue;
    private Boolean enableHtml;

    public static Converter<PropertyUpdateDTO, PropertyDO> converter = new Converter<PropertyUpdateDTO, PropertyDO>() {
        @Override
        public PropertyDO doForward(PropertyUpdateDTO propertyUpdateDTO) {
            return WarpsUtils.copyTo(propertyUpdateDTO, PropertyDO.class);
        }

        @Override
        public PropertyUpdateDTO doBackward(PropertyDO property) {
            return WarpsUtils.copyTo(property, PropertyUpdateDTO.class);
        }
    };

    @Override
    public PropertyDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public PropertyUpdateDTO convertFor(PropertyDO property) {
        return converter.doBackward(property);
    }
}
