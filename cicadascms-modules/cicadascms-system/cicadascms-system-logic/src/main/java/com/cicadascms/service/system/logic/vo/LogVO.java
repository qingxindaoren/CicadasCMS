package com.cicadascms.service.system.logic.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * SysLogVO对象
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SysLogVO对象", description="日志表")
public class LogVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 编号
    */
    @ApiModelProperty(value = "1-编号" )
    private Long id;
    /**
    * 日志类型
    */
    @ApiModelProperty(value = "2-日志类型" )
    private String type;
    /**
    * 日志标题
    */
    @ApiModelProperty(value = "3-日志标题" )
    private String title;
    /**
    * 服务ID
    */
    @ApiModelProperty(value = "4-服务ID" )
    private String clientId;
    /**
    * 创建者
    */
    @ApiModelProperty(value = "5-创建者" )
    private String createBy;
    /**
    * 创建时间
    */
    @ApiModelProperty(value = "6-创建时间" )
    private LocalDateTime createTime;
    /**
    * 操作IP地址
    */
    @ApiModelProperty(value = "7-操作IP地址" )
    private String remoteAddr;
    /**
    * 用户代理
    */
    @ApiModelProperty(value = "8-用户代理" )
    private String userAgent;
    /**
    * 请求URI
    */
    @ApiModelProperty(value = "9-请求URI" )
    private String requestUri;
    /**
    * 操作方式
    */
    @ApiModelProperty(value = "10-操作方式" )
    private String method;
    /**
    * 操作提交的数据
    */
    @ApiModelProperty(value = "11-操作提交的数据" )
    private String params;
    /**
    * 执行时间
    */
    @ApiModelProperty(value = "12-执行时间" )
    private String time;
    /**
    * 异常信息
    */
    @ApiModelProperty(value = "13-异常信息" )
    private String exception;
    /**
    * 所属租户
    */
    @ApiModelProperty(value = "14-所属租户" )
    private Integer tenantId;
    /**
    * 请求时长
    */
    @ApiModelProperty(value = "15-请求时长" )
    private String processTime;

}
