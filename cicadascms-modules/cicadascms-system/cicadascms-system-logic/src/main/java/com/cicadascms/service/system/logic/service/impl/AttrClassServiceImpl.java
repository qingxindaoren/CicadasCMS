package com.cicadascms.service.system.logic.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;

import com.cicadascms.data.domain.AttrClassDO;
import com.cicadascms.data.mapper.AttrClassMapper;

import com.cicadascms.service.system.logic.dto.AttrClassInputDTO;
import com.cicadascms.service.system.logic.dto.AttrClassQueryDTO;
import com.cicadascms.service.system.logic.dto.AttrClassUpdateDTO;
import com.cicadascms.service.system.logic.service.IAttrClassService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.Serializable;

/**
 * <p>
 * 附件分类 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Service("attrClassService")
public class AttrClassServiceImpl extends BaseService<AttrClassMapper, AttrClassDO> implements IAttrClassService {

    @Override
    public R page(AttrClassQueryDTO attrClassQueryDTO) {
        AttrClassDO attrClassDO = attrClassQueryDTO.convertToEntity();
        Page page = baseMapper.selectPage(attrClassQueryDTO.page(), getLambdaQueryWrapper().setEntity(attrClassDO));
        return R.ok(page);
    }

    @Override
    public R save(AttrClassInputDTO attrClassInputDTO) {
        AttrClassDO attrClassDO = attrClassInputDTO.convertToEntity();
        baseMapper.insert(attrClassDO);
        return R.ok(true);
    }

    @Override
    public R update(AttrClassUpdateDTO attrClassUpdateDTO) {
        AttrClassDO attrClassDO = attrClassUpdateDTO.convertToEntity();
        baseMapper.updateById(attrClassDO);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        AttrClassDO attrClassDO = baseMapper.selectById(id);
        return R.ok(attrClassDO);

    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

    @Override
    protected String getCacheName() {
        return "attrClassCache";
    }
}
