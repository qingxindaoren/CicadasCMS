package com.cicadascms.service.system.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.PositionDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * PositionQueryDTO对象
 * 职位表
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="PositionQueryDTO对象")
public class PositionQueryDTO extends BaseDTO<PositionQueryDTO, PositionDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    /**
    * 职位名称
    */
    @ApiModelProperty(value = "3-职位名称" )
    private String postName;
    /**
    * 职位编号
    */
    @ApiModelProperty(value = "4-职位编号" )
    private String postCode;
    /**
    * 职位类型字典表(post_type)
    */
    @ApiModelProperty(value = "5-职位类型字典表(post_type)" )
    private Integer postType;

    public Page<PositionDO> page() {
        Page<PositionDO>  page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<PositionQueryDTO, PositionDO> converter = new Converter<PositionQueryDTO, PositionDO>() {
        @Override
        public PositionDO doForward(PositionQueryDTO positionQueryDTO) {
            return WarpsUtils.copyTo(positionQueryDTO, PositionDO.class);
        }

        @Override
        public PositionQueryDTO doBackward(PositionDO position) {
            return WarpsUtils.copyTo(position, PositionQueryDTO.class);
        }
    };

    @Override
    public PositionDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public PositionQueryDTO convertFor(PositionDO position) {
        return converter.doBackward(position);
    }
}
