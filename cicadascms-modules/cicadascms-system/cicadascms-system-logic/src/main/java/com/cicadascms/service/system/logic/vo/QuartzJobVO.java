package com.cicadascms.service.system.logic.vo;

import com.cicadascms.data.domain.QuartzJobLogDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * SysQuartzJobVO对象
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SysQuartzJobVO对象", description="定时任务")
public class QuartzJobVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 自增主键
    */
    @ApiModelProperty(value = "1-自增主键" )
    private Integer id;
    /**
    * 任务名称
    */
    @ApiModelProperty(value = "2-任务名称" )
    private String jobName;
    /**
    * 任务分组
    */
    @ApiModelProperty(value = "3-任务分组" )
    private String jobGroup;
    /**
    * 执行类
    */
    @ApiModelProperty(value = "4-执行类" )
    private String jobClassName;
    /**
    * cron表达式
    */
    @ApiModelProperty(value = "5-cron表达式" )
    private String cronExpression;
    /**
    * 任务状态
    */
    @ApiModelProperty(value = "6-任务状态" )
    private String triggerState;
    /**
    * 修改之前的任务名称
    */
    @ApiModelProperty(value = "7-修改之前的任务名称" )
    private String oldJobName;
    /**
    * 修改之前的任务分组
    */
    @ApiModelProperty(value = "8-修改之前的任务分组" )
    private String oldJobGroup;
    /**
    * 描述
    */
    @ApiModelProperty(value = "9-描述" )
    private String description;

    private List<QuartzJobLogDO> processList;
}
