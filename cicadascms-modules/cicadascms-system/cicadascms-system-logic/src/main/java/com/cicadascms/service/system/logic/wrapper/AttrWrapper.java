package com.cicadascms.service.system.logic.wrapper;

import cn.hutool.core.lang.Assert;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrDO;
import com.cicadascms.service.system.logic.service.IAttrService;
import com.cicadascms.service.system.logic.vo.AttrVO;

public class AttrWrapper implements BaseWrapper<AttrDO, AttrVO> {

    private final static IAttrService attrService;

    static {
        attrService = SpringContextUtils.getBean(IAttrService.class);
    }

    public static AttrWrapper newBuilder() {
        return new AttrWrapper();
    }

    @Override
    public AttrVO entityVO(AttrDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), AttrDO.class.getName() + " 对象不存在！");
        return WarpsUtils.copyTo(entity, AttrVO.class);
    }
}
