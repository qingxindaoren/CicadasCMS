package com.cicadascms.service.system.logic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.AttrDO;
import com.cicadascms.service.system.logic.dto.AttrInputDTO;
import com.cicadascms.service.system.logic.dto.AttrQueryDTO;
import com.cicadascms.service.system.logic.dto.AttrUpdateDTO;

import java.io.Serializable;

/**
 * <p>
 * 附件 服务类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
public interface IAttrService extends IService<AttrDO> {

    /**
     * 分页方法
     * @param attrQueryDTO
     * @return
     */
    R page(AttrQueryDTO attrQueryDTO);

    /**
     * 保存方法
     * @param attrInputDTO
     * @return
     */
    R save(AttrInputDTO attrInputDTO);

    /**
     * 更新方法
     * @param attrUpdateDTO
     * @return
     */
    R update(AttrUpdateDTO attrUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
