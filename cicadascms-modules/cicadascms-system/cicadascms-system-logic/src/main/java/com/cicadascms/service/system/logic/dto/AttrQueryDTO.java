package com.cicadascms.service.system.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * AttrQueryDTO对象
 * 附件
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="AttrQueryDTO对象")
public class AttrQueryDTO extends BaseDTO<AttrQueryDTO, AttrDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    /**
    * 附件分类
    */
    @ApiModelProperty(value = "3-附件分类" )
    private Integer attrClassId;
    /**
    * 附件名称
    */
    @ApiModelProperty(value = "4-附件名称" )
    private String attrName;
    /**
    * 附件存储类型
    */
    @ApiModelProperty(value = "5-附件存储类型" )
    private Integer attrStoreType;
    /**
    * 附件地址
    */
    @ApiModelProperty(value = "6-附件地址" )
    private String attrLocation;
    /**
    * 创建人
    */
    @ApiModelProperty(value = "7-创建人" )
    private Integer createBy;
    /**
    * 创建时间
    */
    @ApiModelProperty(value = "8-创建时间" )
    private LocalDateTime createTime;

    public Page<AttrDO> page() {
        Page<AttrDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<AttrQueryDTO, AttrDO> converter = new Converter<AttrQueryDTO, AttrDO>() {
        @Override
        public AttrDO doForward(AttrQueryDTO attrQueryDTO) {
            return WarpsUtils.copyTo(attrQueryDTO, AttrDO.class);
        }

        @Override
        public AttrQueryDTO doBackward(AttrDO attrDO) {
            return WarpsUtils.copyTo(attrDO, AttrQueryDTO.class);
        }
    };

    @Override
    public AttrDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AttrQueryDTO convertFor(AttrDO attrDO) {
        return converter.doBackward(attrDO);
    }
}
