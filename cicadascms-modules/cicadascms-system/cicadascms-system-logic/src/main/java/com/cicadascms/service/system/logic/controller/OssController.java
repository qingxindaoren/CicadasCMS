//package com.cicadascms.system.controller;
//
//import com.cicadascms.common.resp.R;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.annotation.Resource;
//
//@Api(tags = "G-云存储上传接口")
//@RestController
//@RequestMapping("/oss")
//public class OssController {
//
//    @Resource
//    private AliYunService aliYunService;
//
//    @ApiOperation(value = "文件上传")
//    @PostMapping("/{bucketName}/upload")
//    public R<OssResult> upload(@ApiParam(name = "bucketName", value = "bucketName", required = true)
//                               @PathVariable String bucketName,
//                               @ApiParam(name = "file", value = "文件", required = true)
//
//                               @RequestParam("file") MultipartFile multipartFile) {
//        return R.ok(aliYunService.uploadFile(multipartFile, bucketName));
//    }
//
//    @ApiOperation(value = "文件上传")
//    @PostMapping("/{bucketName}/access")
//    public R<OssResult> accessUrl(
//            @ApiParam(name = "bucketName", value = "bucketName", required = true)
//            @PathVariable String bucketName, @ApiParam(name = "key", value = "文件key", required = true)
//            @RequestParam("key") String key) {
//        return R.ok(aliYunService.getAccessUrl(key, bucketName));
//    }
//
//}
