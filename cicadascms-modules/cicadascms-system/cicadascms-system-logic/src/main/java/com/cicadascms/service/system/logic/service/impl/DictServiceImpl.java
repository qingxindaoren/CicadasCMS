package com.cicadascms.service.system.logic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.enums.ActiveStateEnum;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.DictDO;
import com.cicadascms.data.mapper.SysDictMapper;
import com.cicadascms.service.system.logic.dto.DictInputDTO;
import com.cicadascms.service.system.logic.dto.DictQueryDTO;
import com.cicadascms.service.system.logic.dto.DictUpdateDTO;
import com.cicadascms.service.system.logic.service.IDictService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-04-07
 */
@Service
public class DictServiceImpl extends BaseService<SysDictMapper, DictDO> implements IDictService {

    @Override
    public R findList(DictQueryDTO dictQueryDTO) {
        LambdaQueryWrapper<DictDO> lambdaQueryWrapper = getLambdaQueryWrapper()
                .setEntity(dictQueryDTO.convertToEntity())
                .orderByAsc(DictDO::getSortId);
        if (Fn.isNull(dictQueryDTO.getParentId())) {
            lambdaQueryWrapper.eq(DictDO::getParentId, Constant.PARENT_ID);
        }
        return R.ok(baseMapper.selectList(lambdaQueryWrapper));
    }

    @Override
    public R page(DictQueryDTO dictQueryDTO) {
        LambdaQueryWrapper<DictDO> lambdaQueryWrapper = getLambdaQueryWrapper().setEntity(dictQueryDTO.convertToEntity());

        if (Fn.isNull(dictQueryDTO.getParentId())) {
            lambdaQueryWrapper.eq(DictDO::getParentId, Constant.PARENT_ID);
        }

        if (Fn.isNotEmpty(dictQueryDTO.getDictName())) {
            lambdaQueryWrapper.like(DictDO::getDictName, dictQueryDTO.getDictName());
        }

        IPage page = page(dictQueryDTO.page(), lambdaQueryWrapper
                .orderByAsc(DictDO::getSortId));

        return R.ok(page);
    }

    @Override
    public R save(DictInputDTO dictInputDTO) {
        DictDO dict = dictInputDTO.convertToEntity();
        if (Fn.isNull(dict.getDictValue()))
            dict.setDictValue(Constant.PARENT_ID);
        return R.ok(save(dict));
    }

    @Override
    public R update(DictUpdateDTO dictUpdateDTO) {
        DictDO dict = dictUpdateDTO.convertToEntity();
        if (Fn.isNull(dict.getDictValue()))
            dict.setDictValue(Constant.PARENT_ID);
        return R.ok(updateById(dict));
    }

    @Override
    public R findById(Serializable id) {
        DictDO dict = getById(id);
        return R.ok(dict);
    }

    @Transactional
    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        baseMapper.delete(getLambdaQueryWrapper().eq(DictDO::getParentId, id));
        return R.ok();
    }

    @Override
    public List<DictDO> findByParentId(Serializable id) {
        return list(getLambdaQueryWrapper()
                .eq(DictDO::getParentId, id)
                .eq(DictDO::getState, ActiveStateEnum.启用.getCode())
                .orderByAsc(DictDO::getSortId)
        );
    }

    @Override
    public List<DictDO> findByCodeAndParentId(String code, Serializable parentId) {
        return list(getLambdaQueryWrapper()
                .eq(DictDO::getParentId, parentId)
                .eq(DictDO::getDictCode, code)
                .eq(DictDO::getState, ActiveStateEnum.启用.getCode())
                .orderByAsc(DictDO::getSortId));
    }

    @Override
    public DictDO findOneByCode(String code) {
        return getOne(getLambdaQueryWrapper()
                .eq(DictDO::getParentId, Constant.PARENT_ID)
                .eq(DictDO::getDictCode, code)
                .eq(DictDO::getState, ActiveStateEnum.启用.getCode())
                .orderByAsc(DictDO::getSortId));
    }

    @Override
    public DictDO findByCodeAndValue(String dictCode, String dictValue) {
        return getOne(getLambdaQueryWrapper()
                .eq(DictDO::getDictCode, dictCode)
                .eq(DictDO::getDictValue, dictValue)
                .eq(DictDO::getState, ActiveStateEnum.启用.getCode())
        );
    }

    @Override
    protected String getCacheName() {
        return "dictCache";
    }

}
