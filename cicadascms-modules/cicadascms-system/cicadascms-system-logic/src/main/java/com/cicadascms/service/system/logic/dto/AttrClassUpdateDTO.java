package com.cicadascms.service.system.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrClassDO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * AttrClassUpdateDTO对象
 * 附件分类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="AttrClassUpdateDTO对象")
public class AttrClassUpdateDTO extends BaseDTO<AttrClassUpdateDTO, AttrClassDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer classId;

    public static Converter<AttrClassUpdateDTO, AttrClassDO> converter = new Converter<AttrClassUpdateDTO, AttrClassDO>() {
        @Override
        public AttrClassDO doForward(AttrClassUpdateDTO attrClassUpdateDTO) {
            return WarpsUtils.copyTo(attrClassUpdateDTO, AttrClassDO.class);
        }

        @Override
        public AttrClassUpdateDTO doBackward(AttrClassDO attrClassDO) {
            return WarpsUtils.copyTo(attrClassDO, AttrClassUpdateDTO.class);
        }
    };

    @Override
    public AttrClassDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AttrClassUpdateDTO convertFor(AttrClassDO attrClassDO) {
        return converter.doBackward(attrClassDO);
    }
}
