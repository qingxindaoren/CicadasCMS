/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 附件
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@TableName("sys_attr")
public class AttrDO implements Serializable {

private static final long serialVersionUID=1L;

                @TableId(value = "id", type = IdType.AUTO)
                private Integer id;

        /**
         * 附件分类
         */
        private Integer attrClassId;

        /**
         * 附件名称
         */
        private String attrName;

        /**
         * 附件存储类型
         */
        private Integer attrStoreType;

        /**
         * 附件地址
         */
        private String attrLocation;

        /**
         * 创建人
         */
        private Integer createBy;

        /**
         * 创建时间
         */
        private LocalDateTime createTime;


    public Integer getId() {
            return id;
            }

        public void setId(Integer id) {
            this.id = id;
            }

    public Integer getAttrClassId() {
            return attrClassId;
            }

        public void setAttrClassId(Integer attrClassId) {
            this.attrClassId = attrClassId;
            }

    public String getAttrName() {
            return attrName;
            }

        public void setAttrName(String attrName) {
            this.attrName = attrName;
            }

    public Integer getAttrStoreType() {
            return attrStoreType;
            }

        public void setAttrStoreType(Integer attrStoreType) {
            this.attrStoreType = attrStoreType;
            }

    public String getAttrLocation() {
            return attrLocation;
            }

        public void setAttrLocation(String attrLocation) {
            this.attrLocation = attrLocation;
            }

    public Integer getCreateBy() {
            return createBy;
            }

        public void setCreateBy(Integer createBy) {
            this.createBy = createBy;
            }

    public LocalDateTime getCreateTime() {
            return createTime;
            }

        public void setCreateTime(LocalDateTime createTime) {
            this.createTime = createTime;
            }

@Override
public String toString() {
        return "Attr{" +
                "id=" + id +
                ", attrClassId=" + attrClassId +
                ", attrName=" + attrName +
                ", attrStoreType=" + attrStoreType +
                ", attrLocation=" + attrLocation +
                ", createBy=" + createBy +
                ", createTime=" + createTime +
        "}";
        }
}
