/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 附件分类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@TableName("sys_attr_class")
public class AttrClassDO implements Serializable {

private static final long serialVersionUID=1L;

                @TableId(value = "class_id", type = IdType.AUTO)
                private Integer classId;


    public Integer getClassId() {
            return classId;
            }

        public void setClassId(Integer classId) {
            this.classId = classId;
            }

@Override
public String toString() {
        return "AttrClass{" +
                "classId=" + classId +
        "}";
        }
}
