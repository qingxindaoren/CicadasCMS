/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 区域 AreaDO
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_area")
public class AreaDO extends BaseDO {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 父编号
     */
    private String parentId;

    /**
     * 简称
     */
    private String shortName;

    /**
     * 级别
     */
    private Integer levelType;

    /**
     * 城市代码
     */
    private String ctyCode;

    /**
     * 邮编
     */
    private String zipCode;

    /**
     * 详细名称
     */
    private String mergerName;

    /**
     * 经度
     */
    private String lng;

    /**
     * 维度
     */
    private String lat;

    /**
     * 拼音
     */
    private String pinyin;

}
