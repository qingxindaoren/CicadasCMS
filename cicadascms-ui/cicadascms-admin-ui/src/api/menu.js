import request from '@/utils/request'
import QueryString from 'qs'

const urlPrefix = '/system/menu'

export function treeList() {
  return request({
    url: urlPrefix + '/tree',
    method: 'get'
  })
}

export function list(query) {
  return request({
    url: urlPrefix + '/list',
    method: 'get',
    params: query,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function(data) {
      return QueryString.stringify(data)
    }]
  })
}

// export function list(param) {
//     return request({
//         url: urlPrefix + '/list',
//         method: 'get',
//         params: param
//     })
// }

export function findById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'get'
  })
}

export function deleteById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'delete'
  })
}

export function save(data) {
  return request({
    url: urlPrefix,
    method: 'post',
    data
  })
}

export function updateById(data) {
  return request({
    url: urlPrefix,
    method: 'put',
    data
  })
}

export function getRouters() {
  return request({
    url: urlPrefix + '/routes'
  })
}
