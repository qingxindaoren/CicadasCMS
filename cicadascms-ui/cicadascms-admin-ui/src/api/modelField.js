import request from '@/utils/request'
import QueryString from 'qs'

const urlPrefix = '/admin/cms/modelField'

export function modelFieldPage(query) {
  return request({
    url: urlPrefix + '/page',
    method: 'get',
    params: query,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function(data) {
      return QueryString.stringify(data)
    }]
  })
}

export function findModelFieldById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'get'
  })
}

export function findModelFieldPropByType(modelType) {
  return request({
    url: urlPrefix + '/type/' + modelType + '/prop',
    method: 'get'
  })
}

export function deleteModelFieldById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'delete'
  })
}

export function saveModelField(data) {
  return request({
    url: urlPrefix,
    method: 'post',
    data
  })
}

export function updateModelFieldById(data) {
  return request({
    url: urlPrefix,
    method: 'put',
    data
  })
}
