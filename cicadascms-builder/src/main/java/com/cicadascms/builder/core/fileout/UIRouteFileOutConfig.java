/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.core.fileout;

import com.cicadascms.builder.core.BaseFileOutConfig;
import com.cicadascms.builder.core.constant.MyConstVal;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;


/**
 * 自定义 UI Route 生成配置
 *
 * @author Jin
 */
public class UIRouteFileOutConfig extends BaseFileOutConfig {

    @Override
    public String getTemplatePath() {
        return MyConstVal.TEMPLATE_UI_ROUTER + ".vm";
    }

    @Override
    public String outputFile(TableInfo tableInfo) {
        String path = System.getProperty("user.dir") + "/vue/";
        String fileName = getLowerFirstNonPrefixName(tableInfo.getName(), tableInfo.getEntityName()) + MyConstVal.JS_SUFFIX;
        return joinPath(path,  "ui.route." ) + fileName;
    }

}
