/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.core.config;

import com.baomidou.mybatisplus.generator.config.PackageConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 自定义包设置
 *
 * @author Jin
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper=true)
public class MyPackageConfig extends PackageConfig {

    /**
     * vo 类包名
     */
    private String vo = "vo";

    /**
     * dto 类包名
     */
    private String dto = "dto";

    /**
     * wrapper 包装类包名
     */
    private String wrapper = "wrapper";


    /**
     * daoPath 路径
     */
    private String daoPath;

}
