/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.enums;


import com.cicadascms.common.func.Fn;

/**
 * WeekEnum
 *
 * @author Jin
 */
public enum WeekEnum {

    MONDAY("MONDAY",1,"星期一"),
    TUESDAY("TUESDAY",2,"星期二"),
    WEDNESDAY("WEDNESDAY",3,"星期三"),
    THURSDAY("THURSDAY",4,"星期四"),
    FRIDAY("FRIDAY",5,"星期五"),
    SATURDAY("SATURDAY",6,"星期六"),
    SUNDAY("SUNDAY",7,"星期天");

    private String code;
    private Integer weekDay;
    private String des;

    public String getCode() {
        return code;
    }

    public Integer getWeekDay() {
        return weekDay;
    }

    public String getDes() {
        return des;
    }

    WeekEnum(String code, Integer weekDay, String des) {
        this.code = code;
        this.weekDay = weekDay;
        this.des = des;
    }

    /**
     * 根据code 得到对应的 周（几）数字
     * @param code
     * @return
     */
    public static Integer getWeekDayByCode(String code) {
        Integer result = null;
        for (WeekEnum order : WeekEnum.values()) {
            if (Fn.equal(order.getCode(),code)) {
                result = order.getWeekDay();
                break;
            }
        }
        return result;
    }

    /**
     * 根据code 得到对应的 周（几）描述
     * @param code
     * @return
     */
    public static String getChineseWeekByCode(String code) {
        String result = null;
        for (WeekEnum order : WeekEnum.values()) {
            if (Fn.equal(order.getCode(),code)) {
                result = order.getDes();
                break;
            }
        }
        return result;
    }

    /**
     * 根据code 得到枚举
     * @param code
     * @return
     */
    public static WeekEnum getEnumByCode(String code) {
        if (Fn.isEmpty(code)) {
            return null;
        }
        for (WeekEnum trans : values()) {
            if (Fn.equal(code,trans.getCode())) {
                return trans;
            }
        }
        return null;
    }

}
