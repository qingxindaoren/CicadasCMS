/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.enums;

import com.cicadascms.common.base.BaseEnum;

/**
 * MenuType
 *
 * @author Jin
 */
public enum MenuType implements BaseEnum<Integer> {
    菜单(1),
    按钮(2);
    public final Integer code;

    MenuType(Integer code) {
        this.code = code;
    }


    @Override
    public Integer getCode() {
        return code;
    }
}
