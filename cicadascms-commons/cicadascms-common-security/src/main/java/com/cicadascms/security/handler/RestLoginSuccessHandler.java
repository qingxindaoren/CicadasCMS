/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.security.handler;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.func.Fn;
import com.cicadascms.security.ClientsUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestValidator;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;


/**
 * RestLoginSuccessHandler
 *
 * @author Jin
 */
@Slf4j
public class RestLoginSuccessHandler implements AuthenticationSuccessHandler {
    private final ObjectMapper objectMapper;
    private final PasswordEncoder passwordEncoder;
    private final ClientDetailsService redisClientDetailsService;
    private final AuthorizationServerTokenServices defaultAuthorizationServerTokenServices;

    public RestLoginSuccessHandler(ObjectMapper objectMapper, PasswordEncoder passwordEncoder, ClientDetailsService redisClientDetailsService, AuthorizationServerTokenServices defaultAuthorizationServerTokenServices) {
        this.objectMapper = objectMapper;
        this.passwordEncoder = passwordEncoder;
        this.redisClientDetailsService = redisClientDetailsService;
        this.defaultAuthorizationServerTokenServices = defaultAuthorizationServerTokenServices;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
            ClientsUtils.ClientInfo client = ClientsUtils.buildClientInfo(request.getHeader(HttpHeaders.AUTHORIZATION));

            ClientDetails clientDetails = redisClientDetailsService.loadClientByClientId(client.getClientId());

            if (Fn.isNull(clientDetails)) {
                throw new UnapprovedClientAuthenticationException("client不存在！");
            } else if (!passwordEncoder.matches(client.getClientSecret(), clientDetails.getClientSecret())) {
                throw new UnapprovedClientAuthenticationException("clientSecret不匹配！");
            }

            TokenRequest tokenRequest = new TokenRequest(MapUtils.EMPTY_SORTED_MAP, client.getClientId(), clientDetails.getScope(), "mobile");
            OAuth2RequestValidator oAuth2RequestValidator = new DefaultOAuth2RequestValidator();
            oAuth2RequestValidator.validateScope(tokenRequest, clientDetails);
            OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);
            OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);
            OAuth2AccessToken oAuth2AccessToken = defaultAuthorizationServerTokenServices.createAccessToken(oAuth2Authentication);
            oAuth2Authentication.setAuthenticated(true);
            response.setCharacterEncoding(Constant.UTF_8);
            response.setContentType(Constant.APPLICATION_JSON);
            PrintWriter printWriter = response.getWriter();
            printWriter.append(objectMapper.writeValueAsString(oAuth2AccessToken));

        } catch (Exception e) {
            throw new UnapprovedClientAuthenticationException(e.getMessage());
        }
    }
}
