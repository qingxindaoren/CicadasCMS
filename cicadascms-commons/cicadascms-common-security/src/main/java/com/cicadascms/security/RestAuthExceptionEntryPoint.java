/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.security;

import cn.hutool.core.util.CharsetUtil;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * RestAuthExceptionEntryPoint
 *
 * @author Jin
 */
@Slf4j
public class RestAuthExceptionEntryPoint implements AuthenticationEntryPoint {

    String accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";

    private final ObjectMapper objectMapper;

    public RestAuthExceptionEntryPoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {
        response.setCharacterEncoding(CharsetUtil.UTF_8);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());

        String accept = request.getHeader("Accept");
        String referer = request.getHeader("Referer");

        if (accept.contains(MediaType.APPLICATION_JSON_VALUE) || Fn.endsWith(referer, "/swagger-ui.html")) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            PrintWriter printWriter = response.getWriter();
            printWriter.append(objectMapper.writeValueAsString(R
                    .error()
                    .setMessage(authException instanceof InsufficientAuthenticationException ? "请求未授权或令牌已过期!（" + authException.getMessage() + "）" : authException.getMessage())));
            printWriter.close();
        } else {
            request.setAttribute(RequestDispatcher.ERROR_MESSAGE, "未授权！");
            request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, HttpStatus.UNAUTHORIZED.value());
            request.getRequestDispatcher("/error").forward(request, response);
        }

    }
}
