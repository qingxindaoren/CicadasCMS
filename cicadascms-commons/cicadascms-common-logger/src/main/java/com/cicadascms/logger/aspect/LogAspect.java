/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.logger.aspect;

import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.data.domain.LogDO;
import com.cicadascms.logger.annotation.ApiLog;
import com.cicadascms.logger.constant.LogTypeEnum;
import com.cicadascms.common.event.LogEvent;
import com.cicadascms.logger.utils.LogUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * LogAspect
 *
 * @author Jin
 */
@Slf4j
@Component
@Aspect
public class LogAspect {

    @Around("@annotation(apiLog)")
    public Object around(ProceedingJoinPoint point, ApiLog apiLog) throws Throwable {
        String strClassName = point.getTarget().getClass().getName();
        String strMethodName = point.getSignature().getName();
        log.debug("[类名]:{},[方法]:{}", strClassName, strMethodName);
        LogDO logDO = LogUtils.buildSysLog(LogTypeEnum.操作日志);
        logDO.setTitle(apiLog.value());
        Long startTime = System.currentTimeMillis();
        Object obj = point.proceed();
        Long endTime = System.currentTimeMillis();
        logDO.setProcessTime(String.valueOf(endTime - startTime));
        SpringContextUtils.publishEvent(new LogEvent(logDO));
        return obj;
    }

}
