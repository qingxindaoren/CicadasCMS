/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.plugin.manager;


import com.cicadascms.common.func.Fn;
import com.cicadascms.plugin.IPlugin;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * PluginsManager
 *
 * @author Jin
 */
public class PluginsManager {

    private final Map<Class<?>, List<IPlugin>> pluginsMap = new HashMap<>();

    public void registerPlugin(IPlugin plugin) {
        Class<?> clazz = plugin.getClass().getSuperclass();
        List<IPlugin> pluginList = pluginsMap.get(clazz);
        if (Fn.isEmpty(pluginList)) {
            pluginList = new LinkedList<>();
        }
        pluginsMap.put(clazz, pluginList);
        pluginList.add(plugin);
    }


    public <T> List<IPlugin> getPlugins(Class<T> clazz) {
        return pluginsMap.get(clazz);
    }

}
