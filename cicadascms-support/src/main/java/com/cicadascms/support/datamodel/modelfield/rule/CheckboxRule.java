/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.modelfield.rule;


import com.cicadascms.support.datamodel.constant.ModelFieldTypeEnum;
import com.cicadascms.support.datamodel.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class CheckboxRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.CHECKBOX_NAME;

    private Boolean require = false;

    private String message = "请选择！";

    private Integer wordLimit = 20;

    private List<Map<String, Object>> element = new ArrayList<>();

    public CheckboxRule() {
        Map<String, Object> demo = new HashMap<>();
        demo.put("label", "label");
        demo.put("value", "value");
        element.add(demo);
    }

}
