/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.modelfield.prop;

import com.cicadascms.support.datamodel.constant.ModelFieldTypeEnum;
import com.cicadascms.support.datamodel.constant.MysqlColumnTypeEnum;
import com.cicadascms.support.datamodel.modelfield.ModelFieldProp;
import com.cicadascms.support.datamodel.modelfield.rule.JsonRule;
import com.cicadascms.support.datamodel.modelfield.value.JsonValue;
import lombok.Getter;
import lombok.Setter;

/**
 * JsonProp
 *
 * @author Jin
 */
@Getter
@Setter
public class JsonProp implements ModelFieldProp<JsonRule, JsonValue> {
    private String name = ModelFieldTypeEnum.JSON_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 1024;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private JsonRule rule = new JsonRule();
    private JsonValue value = new JsonValue();

}
