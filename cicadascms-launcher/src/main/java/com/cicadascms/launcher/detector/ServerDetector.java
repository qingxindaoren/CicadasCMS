/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.detector;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServerDetector {

    public static final String GERONIMO_ID = "geronimo";

    public static final String GLASSFISH_ID = "glassfish";

    public static final String JBOSS_ID = "jboss";

    public static final String JETTY_ID = "jetty";

    public static final String JONAS_ID = "jonas";

    public static final String OC4J_ID = "oc4j";

    public static final String RESIN_ID = "resin";

    public static final String TOMCAT_ID = "tomcat";

    public static final String UNDERTOW_ID = "undertow";

    public static final String WEB_LOGIC_ID = "weblogic";

    public static final String WEBSPHERE_ID = "websphere";

    public static String getServerId() {
        ServerDetector sd = _instance;

        if (sd._serverId == null) {
            if (isGeronimo()) {
                sd._serverId = GERONIMO_ID;
            } else if (isGlassfish()) {
                sd._serverId = GLASSFISH_ID;
            } else if (isJBoss()) {
                sd._serverId = JBOSS_ID;
            } else if (isJOnAS()) {
                sd._serverId = JONAS_ID;
            } else if (isOC4J()) {
                sd._serverId = OC4J_ID;
            } else if (isResin()) {
                sd._serverId = RESIN_ID;
            } else if (isWebLogic()) {
                sd._serverId = WEB_LOGIC_ID;
            } else if (isWebSphere()) {
                sd._serverId = WEBSPHERE_ID;
            } else if (isJetty()) {
                if (sd._serverId == null) {
                    sd._serverId = JETTY_ID;
                } else {
                    sd._serverId += "-" + JETTY_ID;
                }
            } else if (isTomcat()) {
                if (sd._serverId == null) {
                    sd._serverId = TOMCAT_ID;
                } else {
                    sd._serverId += "-" + TOMCAT_ID;
                }
            } else if (isUndertow()) {
                if (sd._serverId == null) {
                    sd._serverId = UNDERTOW_ID;
                } else {
                    sd._serverId += "-" + UNDERTOW_ID;
                }
            }

            if (log.isInfoEnabled()) {
                if (sd._serverId != null) {
                    log.info("Detected server " + sd._serverId);
                } else {
                    log.info("No server detected");
                }
            }

            if (sd._serverId == null) {
                throw new RuntimeException("Server is not supported");
            }
        }

        return sd._serverId;
    }

    public static boolean isGeronimo() {
        ServerDetector sd = _instance;

        if (sd._geronimo == null) {
            sd._geronimo = _detect(
                    "/org/apache/geronimo/system/main/Daemon.class");
        }

        return sd._geronimo;
    }

    public static boolean isGlassfish() {
        ServerDetector sd = _instance;

        if (sd._glassfish == null) {
            String value = System.getProperty("com.sun.aas.instanceRoot");

            if (value != null) {
                sd._glassfish = Boolean.TRUE;
            } else {
                sd._glassfish = Boolean.FALSE;
            }
        }

        return sd._glassfish;
    }

    public static boolean isGlassfish2() {
        ServerDetector sd = _instance;

        if (sd._glassfish2 == null) {
            if (isGlassfish() && !isGlassfish3()) {
                sd._glassfish2 = Boolean.TRUE;
            } else {
                sd._glassfish2 = Boolean.FALSE;
            }
        }

        return sd._glassfish2;
    }

    public static boolean isGlassfish3() {
        ServerDetector sd = _instance;

        if (sd._glassfish3 == null) {
            String value = "";

            if (isGlassfish()) {
                value = System.getProperty("product.name");
            }

            if (value.equals("GlassFish/v3")) {
                sd._glassfish3 = Boolean.TRUE;
            } else {
                sd._glassfish3 = Boolean.FALSE;
            }
        }

        return sd._glassfish3;
    }

    public static boolean isJBoss() {
        ServerDetector sd = _instance;

        if (sd._jBoss == null) {
            sd._jBoss = _detect("/org/jboss/Main.class");
        }

        return sd._jBoss;
    }

    public static boolean isJetty() {
        ServerDetector sd = _instance;

        if (sd._jetty == null) {
            sd._jetty = _detect("/org/mortbay/jetty/Server.class");
        }

        return sd._jetty;
    }

    public static boolean isJOnAS() {
        ServerDetector sd = _instance;

        if (sd._jonas == null) {
            sd._jonas = _detect("/org/objectweb/jonas/server/Server.class");
        }

        return sd._jonas;
    }

    public static boolean isOC4J() {
        ServerDetector sd = _instance;

        if (sd._oc4j == null) {
            sd._oc4j = _detect("oracle.oc4j.util.ClassUtils");
        }

        return sd._oc4j;
    }

    public static boolean isResin() {
        ServerDetector sd = _instance;

        if (sd._resin == null) {
            sd._resin = _detect("/com/caucho/server/resin/Resin.class");
        }

        return sd._resin;
    }

    public static boolean isTomcat() {
        ServerDetector sd = _instance;

        if (sd._tomcat == null) {
            sd._tomcat = _detect(
                    "/org/apache/catalina/startup/Bootstrap.class");
        }

        if (sd._tomcat == null) {
            sd._tomcat = _detect("/org/apache/catalina/startup/Embedded.class");
        }

        return sd._tomcat;
    }

    public static boolean isUndertow() {
        ServerDetector sd = _instance;

        if (sd._undertow == null) {
            sd._undertow = _detect(
                    "/io/undertow/Undertow.class");
        }
        return sd._undertow;
    }

    public static boolean isWebLogic() {
        ServerDetector sd = _instance;

        if (sd._webLogic == null) {
            sd._webLogic = _detect("/weblogic/Server.class");
        }

        return sd._webLogic;
    }

    public static boolean isWebSphere() {
        ServerDetector sd = _instance;

        if (sd._webSphere == null) {
            sd._webSphere = _detect(
                    "/com/ibm/websphere/product/VersionInfo.class");
        }

        return sd._webSphere;
    }

    private static Boolean _detect(String className) {
        try {
            ClassLoader.getSystemClassLoader().loadClass(className);

            return Boolean.TRUE;
        } catch (ClassNotFoundException cnfe) {

            Class<?> c = _instance.getClass();

            if (c.getResource(className) != null) {
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        }
    }

    private ServerDetector() {
    }

    private static final ServerDetector _instance = new ServerDetector();

    private String _serverId;
    private Boolean _geronimo;
    private Boolean _glassfish;
    private Boolean _glassfish2;
    private Boolean _glassfish3;
    private Boolean _jBoss;
    private Boolean _jetty;
    private Boolean _jonas;
    private Boolean _oc4j;
    private Boolean _resin;
    private Boolean _tomcat;
    private Boolean _undertow;
    private Boolean _webLogic;
    private Boolean _webSphere;

}
