/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.config.swagger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;


/**
 * SwaggerConfig
 *
 * @author Jin
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Value("${cicadascms.name}")
    private String name;

    @Value("${cicadascms.version}")
    private String version;

    @Value("${cicadascms.http.url}")
    private String host;

    @Bean
    public Docket frontApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("前台业务接口")
                .apiInfo(apiInfo())
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.cicadascms.service.front.logic.controller.api"))
                .build()
                .pathMapping("/");
    }

    @Bean
    public Docket systemApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("系统管理接口")
                .apiInfo(apiInfo())
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.cicadascms.service.system.logic.controller"))
                .build()
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(securitySchema()))
                .pathMapping("/system");
    }

    @Bean
    public Docket adminApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("后台管理接口")
                .apiInfo(apiInfo())
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.cicadascms.service.admin.logic.controller"))
                .build()
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(securitySchema()))
                .pathMapping("/admin");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(Collections.singletonList(SecurityReference.builder()
                        .reference(securitySchema().getName())
                        .scopes(securitySchema().getScopes().toArray(new AuthorizationScope[0]))
                        .build()))
                .forPaths(PathSelectors.regex("^.*$"))
                .build();
    }

    private OAuth securitySchema() {
        return new OAuth("认证接口",
                Collections.singletonList(new AuthorizationScope("all", "全部")),
                Collections.singletonList(new ResourceOwnerPasswordCredentialsGrant(host + "/oauth/token")));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(name)
                .description(name + "接口文档")
                .license(name)
                .licenseUrl("")
                .termsOfServiceUrl("")
                .contact(new Contact("Jin", "", ""))
                .version(version)
                .build();
    }

}

