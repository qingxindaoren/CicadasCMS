/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.config.freemaker;


import com.cicadascms.common.utils.ResUtils;
import com.cicadascms.launcher.detector.WebDirDetector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

/**
 * FreeMakerConfig
 *
 * @author Jin
 */
@Slf4j
@Configuration
public class FreeMakerConfig {

    private static final String TEMPLATES_FOLDER = "templates";

    @Bean
    public FreeMarkerConfigurer freemarkerConfig() {
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setDefaultEncoding("UTF-8");
        freeMarkerConfigurer.setPreferFileSystemAccess(false);
        //忽略IDE启动项目
        if (WebDirDetector.isStartupFromJar() || WebDirDetector.isStartupFromContainer()) {
            //创建模板文件目录
            String templatesPath = ResUtils.checkAndCreateResourceDir(TEMPLATES_FOLDER);
            log.info("========[ JAR方式运行设置模板路径至 - {} ]========", templatesPath);
            freeMarkerConfigurer.setTemplateLoaderPath("file:" + templatesPath + "/");
        } else {
            freeMarkerConfigurer.setTemplateLoaderPath("classpath:/templates/");
        }
        return freeMarkerConfigurer;
    }


    @Bean
    public FreeMarkerViewResolver viewResolver() {
        FreeMarkerViewResolver freeMarkerViewResolver = new FreeMarkerViewResolver();
        freeMarkerViewResolver.setContentType("text/html;charset=UTF-8");
        freeMarkerViewResolver.setOrder(0);
        freeMarkerViewResolver.setSuffix(".html");
        freeMarkerViewResolver.setCache(false);
        return freeMarkerViewResolver;
    }
}
